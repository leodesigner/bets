var APP = APP || {}
APP.Bets = {
    setUp: function(){
        this.selectBet();
        this.slickBets();
        this.removerAposta();
        this.editarPerfil();
        this.editarBanco();
        this.deletarConta();
        this.editarSenha();
        this.sacarBet();
        this.bankSucesso();
        this.showImage();
        this.depositarBet();
        this.showSenha();
        this.checkAposta();
    },
    //ShowImage
    showImage: function(){
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
        
                reader.onload = function (e) {
                    $('#minImg').attr('src', e.target.result);
                }
        
                reader.readAsDataURL(input.files[0]);
            }
        }
        
        $("#avatar").change(function(){
            readURL(this);
        });
    },

    //Select
    selectBet: function(){
        $('select').selectpicker();
    },
    //Jogos em destaque home - slick
    slickBets: function(){        
        $('.bets-d-home').slick({
            arrows: true,
            infinite: false,
            centerPadding: '32px',
            slidesToShow: 2.3,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 1366,
                    setings: {
                        slidesToShow: 1.7,
                        slidesToScroll: 1                
                    }
                },
                {breakpoint: 1920,
                    setings: {
                        slidesToShow: 1.7,
                        slidesToScroll: 1                
                    }
                }
            ]
        });
    },
    //Marcar aposta
    checkAposta: function(){
        $('.odds a.odd').on("click", function(){
            $(this).toggleClass('active');
            return false;
        });
    },
    //Remover aposta
    removerAposta: function(){
        $('.delet-bet').on("click", function(){
            $('#removerAposta').modal('show');
        });
    },
    //Editar perfil
    editarPerfil: function(){
        $('.edit-perfil').on("click", function(){
            $('#editarPerfil').modal('show');
        });
        $('.conf-edit-per').on("click", function(){
            $('#editarPerfil').modal('hide');
            $('#sucessoPerfil').modal('show');
        });
    },
    //Editar banco
    editarBanco: function(){
        $('.edit-banco').on("click", function(){
            $('#editarBanco').modal('show');
        });
        $('.conf-edit-bank').on("click", function(){
            $('#editarBanco').modal('hide');
            $('#sucessoBanco').modal('show');
        });
    },
    //Deletar conta bancária
    deletarConta: function(){
        $('.deletConta').on("click", function(){
            $('#deletarConta').modal('show');
        });
        $('.conf-delet-conta').on("click", function(){
            $('#deletarConta').modal('hide');
            $('#sucessoConta').modal('show');
        });
    },
    //Editar senha
    editarSenha: function(){
        $('.edit-senha').on("click", function(){
            $('#editarSenha').modal('show');
        });
        $('.conf-edit-sen').on("click", function(){
            $('#editarSenha').modal('hide');
            $('#sucessoSenha').modal('show');
        });
    },   
    //Sacar
    sacarBet: function(){
        $('.btSacar').on("click", function(){
            $('#sacarBet').modal('show');
        });
        $('.conf-saque').on("click", function(){
            $('#sacarBet').modal('hide');
            $('#sucessoSacar').modal('show');
        });
    },
    //Depositar
    depositarBet: function(){
        $('.btDepositar').on("click", function(){
            $('#sucessoDepo').modal('show');
        });        
    },  
    //Banco sucesso
    bankSucesso: function(){
        $('.add-bank').on("click", function(){
            $('#sucessoAddconta').modal('show');
        });
    },
    //Mostrar senha
    showSenha: function(){
        $('.show-pass').on("click", function(){
            if ($(".pass").attr("type") === "password") {
                $(".pass").attr("type", "text");
            } else {
                $(".pass").attr("type", "password");
            }
            return false;
        });
    },
};
(function(){
    APP.Bets.setUp();
})();
